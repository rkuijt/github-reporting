import csv
import os

from domain.github_credentials import GitHubCredentials
from domain.github_gateway import GitHubGateway


GITHUB_ORG = str(os.environ.get('GITHUB_ORG'))
GITHUB_USERNAME = str(os.getenv('GITHUB_USERNAME'))
GITHUB_TOKEN = str(os.environ.get('GITHUB_TOKEN'))

gateway = GitHubGateway(GITHUB_ORG, GitHubCredentials(GITHUB_USERNAME, GITHUB_TOKEN))
org_data = gateway.get_org_repositories()

with open('./output/repositories.csv', 'w', newline='') as csvfile:
    csv_writer = csv.writer(csvfile, delimiter=';', quotechar='\"', quoting=csv.QUOTE_MINIMAL)
    csv_writer.writerow(["Name", "Full Name", "Description", "Created at", "Pushed at", "Updated at", "URL", "Size in Kb"])
    for repo in org_data.repositories:
        csv_writer.writerow([repo.name, repo.full_name, repo.description, repo.created_at, repo.pushed_at, repo.updated_at, repo.html_url, repo.size_kb])

print(f"Total repository size: {org_data.get_total_size_kb()}Kb")
