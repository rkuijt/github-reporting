from dataclasses import dataclass
from datetime import datetime
from typing import List


@dataclass
class Repository:
    """A DTO containing the received information for a GitHub repository."""
    name: str               # TCTI-V1OOPC-15-BASE
    full_name: str          # huict/TCTI-V1OOPC-15-BASE
    description: str        # De basis repository voor de cursus TCTI-V1OOPC-15
    html_url: str           # https://api.github.com/repos/huict/TCTI-V1OOPC-15-BASE
    created_at: datetime    # 2016-04-10T17:30:16Z
    updated_at: datetime    # 2016-05-12T14:32:39Z
    pushed_at: datetime     # 2016-06-05T13:26:41Z
    size_kb: int            # 11435


@dataclass
class Org:
    org_id: str                       # huict
    repositories: List[Repository]

    def get_total_size_kb(self) -> int:
        total_size_kb = 0
        for repo in self.repositories:
            total_size_kb += repo.size_kb
        return total_size_kb
