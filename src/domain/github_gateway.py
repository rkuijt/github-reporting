import re
import time

import os
import requests
from requests.adapters import HTTPAdapter
from urllib3 import Retry

from src.domain.github_credentials import GitHubCredentials
from src.domain.github_data import Org, Repository


class UnsuccessfulGitHubResponse(Exception):
    """Raised when GitHub does not reply with a successful status code after retries."""
    pass


class GitHubGateway:
    """Responsible for communicating with the GitHub API."""
    API_URL_TEMPLATE = "https://api.github.com/orgs/{}/repos"
    SECONDS_BETWEEN_REQUESTS = 3

    def __init__(self, org_id: str, credentials: GitHubCredentials):
        self.org_id = org_id
        self.credentials = credentials

        # Setup requests session
        self.session = requests.Session()
        retries = Retry(total=5, backoff_factor=2, status_forcelist=[500, 502, 503, 504])
        self.session.mount("https://", HTTPAdapter(max_retries=retries))

    def get_org_repositories(self) -> Org:
        print(f"Retrieving data for organization {self.org_id}", end=os.linesep)
        current_page = 1
        current_url = self.API_URL_TEMPLATE.format(self.org_id)
        repositories = []

        while True:
            response = self._request_url(current_url)
            for repo_json in response.json():
                repositories.append(Repository(
                    name=repo_json.get("name"),
                    full_name=repo_json.get("full_name"),
                    description=repo_json.get("description"),
                    html_url=repo_json.get("html_url"),
                    created_at=repo_json.get("created_at"),
                    updated_at=repo_json.get("updated_at"),
                    pushed_at=repo_json.get("pushed_at"),
                    size_kb=repo_json.get("size"),
                ))

            # Retrieve the last page number in order to show progress.
            last_page_link = response.links.get("last")
            if last_page_link is not None:
                last_page = int(re.search("\\?page=(\\d+)", last_page_link.get("url")).group(1))
                print(f"\rParsed page {current_page}/{last_page}", end="")
            current_page += 1

            # Retrieve the next page url, or stop if there is no next url.
            next_page_link = response.links.get("next")
            if next_page_link is None:
                print(f"\rParsed page {current_page}/{current_page}", end=os.linesep)
                break
            current_url = next_page_link.get("url")

            time.sleep(self.SECONDS_BETWEEN_REQUESTS)

        return Org(self.org_id, repositories)

    def _request_url(self, url: str) -> requests.Response:
        response = self.session.get(url, allow_redirects=True, auth=(self.credentials.username, self.credentials.token))
        if not response.ok:
            raise UnsuccessfulGitHubResponse(f"Received {response.status_code} from GitHub API. Response text: \n{response.text}")
        return response
