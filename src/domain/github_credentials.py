from dataclasses import dataclass


@dataclass
class GitHubCredentials:
    """A class representing a GitHub username and token."""
    username: str
    token: str
