FROM python:3.11-slim-bullseye

RUN mkdir /app /app/output
WORKDIR /app
COPY ./src/ poetry.lock pyproject.toml ./
RUN pip install poetry --disable-pip-version-check && \
    poetry config virtualenvs.create false && \
    poetry install --only main --no-interaction --no-ansi

ENTRYPOINT ["poetry", "run", "python", "-u", "main.py"]
