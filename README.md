# github-reporting
A tool for querying GitHub organization repo usage stats.

## Usage
Docker is the most straightforward way to use this tool:
```shell
docker run --rm -e GITHUB_ORG=<YOUR-GITHUB-ORG> \
                -e GITHUB_USER=<YOUR-GITHUB-USERNAME> \
                -e GITHUB_TOKEN=<YOUR-GITHUB-TOKEN> \
                -v "/full/path/to/output/dir/on/host/:/app/output/" \
                rkuijt/github-reporting:0.1.0
```

You can find the output csv file in the output folder.

## Development
This project uses Poetry for dependency management.
In order to download the dependencies and run through python directly.
```shell
poetry install
poetry run python -u main.py
```

IDE's like IntelliJ should have automatic run configuration integrations.
